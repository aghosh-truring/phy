#!/bin/bash

## IPERF LOOP #################################################################
function do_iperf_loop {
	local Q=${1}
	while :
	do 
		# ip netns exec D${Q} iperf3 -n 1600k -R -c 2.2.2.2
		ip netns exec D${Q} scp -i /home/dummy/.ssh/id_rsa dummy@2.2.2.2:/home/largefiles/10MB.zip /dev/null
		N=$(( 31 + $RANDOM % 60 )) 
		sleep $N
		# ip netns exec D${Q} iperf3 -n 450k -c 2.2.2.2
		ip netns exec D${Q} scp -i /home/dummy/.ssh/id_rsa /home/largefiles/10MB.zip dummy@2.2.2.2:/dev/null
		N=$(( 31 + $RANDOM % 60 ))
		sleep $N 
	done
}

## Connect ####################################################################
function do_connect {
	local Q=${1}
	local BULK=${2}
	local SSID="foobar"

	if [ -d /phy/device/${Q}/user ]
	then
		if [ -f /phy/device/${Q}/user/ssid ]
		then
			SSID=`cat /phy/device/${Q}/user/ssid`
		fi

		/phy/eapol_test -M  `cat /phy/device/${Q}/mac`  -N 30:s:88-88-22-22-22-22:${SSID} -c /phy/device/${Q}/user/conf -a 10.0.254.1 | tail -1
	fi

	if [ -f /phy/device/${Q}/hostname ]
	then
		DEVNAME=`cat /phy/device/${Q}/hostname`
	else
		DEVNAME=D${Q}${Q}${Q}
	fi

	if [ "${BULK}" == "yes" ]
	then
		ip netns exec D${Q} dhcpcd -L -C resolv.conf -4 -A -h ${DEVNAME} F${Q}
	else
		ip netns exec D${Q} dhcpcd -L -C resolv.conf -4 -h ${DEVNAME} F${Q}
	fi
}

## Release ####################################################################
function do_release {
	local Q=${1}

	if [ -f /run/dhcpcd-F${Q}.pid ]
	then
		ip netns exec D${Q} dhcpcd -k F${Q}
	elif [ -f /run/dhcpcd-F${Q}-4.pid ]
	then
		echo "Manual stop in F${Q}"
		ip netns exec D${Q} kill -SIGALRM `cat /run/dhcpcd-F${Q}-4.pid`
		sleep 2
		ip netns exec D${Q} ip address flush dev F${Q} scope global

		if [ -f /run/dhcpcd-F${Q}-4.pid ]
		then
			kill -SIGKILL `cat /run/dhcpcd-F${Q}-4.pid`
			rm -f /run/dhcpcd-F${Q}-4.pid
		fi
	else
		# Hail Mary full of grace...
		ip netns exec D${Q} dhcpcd -k F${Q}
	fi
}

## Load #######################################################################
function do_load {
	local Q=${1}
	local BULK=${2}

	if [ "${BULK}" == "yes" ]
	then
		# ip netns exec D${Q} wget -O /dev/null -q -b http://speedtest.tele2.net/10GB.zip
		ip netns exec D${Q} wget -O /dev/null -q -b http://2.2.2.2/files/10GB.zip
	else
		# ip netns exec D${Q} wget -O /dev/null http://speedtest.tele2.net/10GB.zip
		ip netns exec D${Q} wget -O /dev/null http://2.2.2.2/files/10GB.zip
	fi
}

## Loop #######################################################################
function do_loop {
	local Q=${1}
	local BULK=${2}

	if [ "${BULK}" == "yes" ]
	then
		(do_iperf_loop ${Q}) >/dev/null 2>&1 &
	else
		do_iperf_loop ${Q}
	fi
}

## Status #####################################################################
function do_status {
	local Q=${1}

	ip netns exec D${Q} ip -br a show dev F${Q}
}

## Usage ######################################################################
function do_usage {
	echo "Usage: "
	echo "   ${0} status"
	echo "   ${0} connect|release|load|loop|status  all"
	echo "   ${0} connect|release|load|loop|status  <dev> [<dev> ...]"
}

###############################################################################
## Script #####################################################################
###############################################################################
if [ "$#" -eq 0 ]
then
	do_usage
	exit 1
fi

if [ "$#" -eq 1 ]
then
	if [ "${1}" == "status" ]
	then
		cd /phy/device
		for Q in *
		do
			do_status ${Q} "yes"
		done
		exit 0
	else
		do_usage
		exit 1
	fi
else
	case "${1}" in
		"connect") OP="do_connect"; shift ;;
		"release") OP="do_release"; shift ;;
		"load")    OP="do_load"; shift ;;
		"loop")    OP="do_loop"; shift ;;
		"status")  OP="do_status"; shift ;;
		*) echo "${1} unknown"; do_usage; exit 2;;
	esac
fi

if [ "$#" -gt 1 ]
then
	BULK="yes"
elif [ "${1}" == "all" ]
then
	BULK="yes"
else
	BULK="no"
fi

# set -x 

if [ "${1}" == "all" ]
then
	cd /phy/device

	for Q in *
	do
		${OP} ${Q} ${BULK}
	done
else
	for Q in $*
	do
		if [ ! -d /phy/device/${Q} ]
		then
			echo "No such device ${Q}"
		else
			${OP} ${Q} ${BULK}
		fi
	done
fi
