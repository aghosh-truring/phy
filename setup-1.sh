#!/bin/bash

set -x

SYSCTL=/usr/sbin/sysctl
IP=/usr/sbin/ip
TC=/usr/sbin/tc
IPTABLES=/usr/sbin/iptables
BUSYBOX=/usr/bin/busybox
IPERF=/usr/bin/iperf3

if [ -f /sim.conf ]
then
	source /sim.conf
else
	INET_IFC=eno2
	WAN_IFC=(enp2s0f0 enp2s0f1 enp2s0f2 enp2s0f3)
fi

# Common Part
${SYSCTL} -w net.ipv4.ip_forward=1

# Set up NAT routing
${IPTABLES} -t nat -A POSTROUTING -o ${INET_IFC} -j MASQUERADE

# Set up Local Internet!
${IP} link add mynet type dummy
${IP} addr add 2.2.2.2/32 dev mynet
${IP} link set mynet up

# Start IPERF Server
${IPERF} -s -B 2.2.2.2 -D

for N in 0 1 2 3
do
	${IP} netns add S${N}
	${IP} netns exec S${N} ${IP} addr add 127.0.0.1/8 dev lo
	${IP} netns exec S${N} ${IP} link set lo up
	${IP} netns exec S${N} ${IP} link add name T${N} type bridge
		# ${IP} netns exec S${N} ${SYSCTL} -w net.ipv4.conf.T${N}.arp_ignore=8
	${IP} netns exec S${N} ${IP} link set T${N} up

	${IP} link add W${N} type veth peer name U${N}
	${IP} link set W${N} up

	GW=`grep router /phy/dhcp-1/W${N}.conf | awk '{print $3}'`
	${IP} addr add ${GW}/24 dev W${N}
	${BUSYBOX} udhcpd /phy/dhcp-1/W${N}.conf

	${IP} link set U${N} netns S${N}
	${IP} netns exec S${N} ${IP} link set U${N} up
	${IP} netns exec S${N} ${IP} link set U${N} master T${N}

	${IP} link set ${WAN_IFC[N]} netns S${N}
	${IP} netns exec S${N} ${IP} link set ${WAN_IFC[N]} up
	${IP} netns exec S${N} ${IP} link set ${WAN_IFC[N]} master T${N}
done

# WAN impairments
# Wired 
${IP} netns exec S0 ${TC} qdisc add dev ${WAN_IFC[0]} root netem delay 1ms rate 50mbit 
${IP} netns exec S0 ${TC} qdisc add dev U0   root netem delay 1ms rate 50mbit 

# Wi-Fi
${IP} netns exec S1 ${TC} qdisc add dev ${WAN_IFC[1]} root netem delay 1ms rate 50mbit
${IP} netns exec S1 ${TC} qdisc add dev U1   root netem delay 1ms rate 50mbit

# LTE 
${IP} netns exec S2 ${TC} qdisc add dev ${WAN_IFC[2]} root netem delay 15ms rate 20mbit
${IP} netns exec S2 ${TC} qdisc add dev U2   root netem delay 15ms rate 20mbit

# LTE
${IP} netns exec S3 ${TC} qdisc add dev ${WAN_IFC[3]} root netem delay 15ms rate 20mbit
${IP} netns exec S3 ${TC} qdisc add dev U3   root netem delay 15ms rate 20mbit

## VSAT -- 250ms, 10 Mb/s, 2%
#${IP} netns exec S3 ${TC} qdisc add dev enp2s0f3 root netem delay 250ms rate 10mbit loss 2%
#${IP} netns exec S3 ${TC} qdisc add dev U3   root netem delay 250ms rate 10mbit loss 2%

