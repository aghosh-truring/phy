#!/bin/bash

set -x

SYSCTL=/usr/sbin/sysctl
IP=/usr/sbin/ip
TC=/usr/sbin/tc
IPTABLES=/usr/sbin/iptables
BUSYBOX=/usr/bin/busybox
IPERF=/usr/bin/iperf3

if [ -f /sim.conf ]
then
	source /sim.conf
else
	LAN_IFC=eno1
fi

NOW=`date +%Y-%m-%d-%H-%M-%S`
LOG='/root/setup-lan.log.'${NOW}

set -x
exec 1<&-
exec 2<&-
exec 1<>${LOG}
exec 2>&1

# The LAN Side
${IP} addr add 10.0.254.3/24 dev ${LAN_IFC}
${IP} link set ${LAN_IFC} up

for V in 4093 10 20 30 40 50 60 70 80 90
do
	${IP} link add link ${LAN_IFC} name V${V} type vlan id ${V}
	${IP} link add name B${V} type bridge
	${IP} link set V${V} master B${V}
	${SYSCTL} -w net.ipv4.conf.B${V}.arp_ignore=8

	${IP} link set B${V} up
	${IP} link set V${V} up
done

for n in 0 1 2
do
	V=$(( n + 4000 ))
	${IP} netns add R${V}
	${IP} link add link ${LAN_IFC} name V${V} type vlan id ${V}
	${IP} link set V${V} netns R${V}

	${IP} netns exec R${V} ${IP} addr add 127.0.0.1/8 dev lo
	${IP} netns exec R${V} ${IP} link set lo up
	${IP} netns exec R${V} ${IP} link set V${V} up
	${IP} netns exec R${V} sysctl -w net.ipv4.ip_forward=1

	for m in 0 1 2
	do
		W=2${n}${m}

		${IP} link add name B${W} type bridge
		${IP} link add E${W} type veth peer name F${W}
		${IP} link set E${W} master B${W}
		${IP} link set B${W} up
		${IP} link set E${W} up
		${IP} link set F${W} netns R${V}
		${IP} netns exec R${V} ${IP} link set F${W} up
	done
done

# The Devices
cd /phy/device

for Q in *
do
	${IP} netns add D${Q}
	${IP} link add E${Q} type veth peer name F${Q}

	if [ -f /phy/device/${Q}/vlan ]
	then
		${IP} link set E${Q} master B`cat /phy/device/${Q}/vlan`
	else
		${IP} link set E${Q} master B4093
	fi
	${IP} link set E${Q} up

	# after this lin we can't see interface F in the main namespace
	${IP} link set F${Q} netns D${Q}

	if [ -f /phy/device/${Q}/mac ]
	then
		MAC=`cat /phy/device/${Q}/mac`
	else
		MAC="02:CA:FE:00:00:${Q}"
		echo ${MAC} > /phy/device/${Q}/mac
	fi

	${IP} netns exec D${Q} ${IP} link set F${Q} addr ${MAC}
	${IP} netns exec D${Q} ${IP} link set F${Q} up

	${IP} netns exec D${Q} ${IP} addr add 127.0.0.1/8 dev lo
	${IP} netns exec D${Q} ${IP} link set lo up
done

